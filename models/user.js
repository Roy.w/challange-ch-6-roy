'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User.hasMany(models.User_History,{foreignKey: "user_id", as: "history"});
      models.User_History.belongsTo(models.User,{
        foreignKey: "user_id",
        as: "userHistory",
      });
      models.User.hasOne(models.User_Biodata,{
        foreignKey: "user_id",
        as: "userBiodata"
      })
    }
  }
  User.init({
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};