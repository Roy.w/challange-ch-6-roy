'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_Biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User_Biodata.belongsTo(models.User,{
        foreignKey: "user_id",
        as: "userBiodata",
      })
    }
  }
  User_Biodata.init({
    address: DataTypes.STRING,
    country: DataTypes.STRING,
    city: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User_Biodata',
  });
  return User_Biodata;
};