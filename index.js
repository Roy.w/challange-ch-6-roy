const express = require("express")
const app = express()
const { User,User_History,User_Biodata} = require('./models')
app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.post('/user',(req,res)=>{
    const {name,password,email,address,country,city,result,score} = req.body
    User.create({
        name,
        password,
        email
    })
    .then((user)=>{
        User_History.create({
            result,
            score,
            user_id:user.id,
        }).then((user)=>{
            console.log({message: "create success", data:user})
        });
        User_Biodata.create({
            address,
            country,
            city,
            user_id:user.id
        }).then((user)=>{
            console.log({message: "create success", data: user})
        });
        res.json({ message: "Create Succes", data: user})
    })
})

app.get('/all',(req,res)=>{
    User.findAll({include: ["history", "userBiodata"]}).then((users)=>{
        res.json({message: "fetch all success",data: users})
    })
})

app.get('/all/:id',(req,res)=>{
    const {id} = req.params;
    User.findOne({include:["history","userBiodata"],where:{id:id}}).then((users)=>{
        res.json({message: "fetch all success",data: users})
    })
})

app.delete("/all/:id",(req,res)=>{
    const {id} = req.params;
    User.destroy({where: {id: id}})
    .then(()=>{
        User_History.destroy({
            where: {user_id:id}
        })
        User_Biodata.destroy({
            where: {user_id:id}
        })     
        res.json({message:"delete success"})
    })
    .catch(()=>{
        res.json({message:"gagal menghapus data"})
    })
})

app.put("/all/:id",(req,res)=>{
    const {id} = req.params;
    const {name,password,email,address,country,city} = req.body;
    User.update({
        name,
        password,
        email
        },{where: {id:id}})
    .then(()=>{
        User_Biodata.update({
            address,
            country,
            city
        },{where:{user_id:id}})
        res.json({message: "update success"})
    })
    .catch(()=>{
        res.json({message:"gagal mengupdate artikel"})
    })
})

app.get('/history',(req,res)=>{
    User_History.findAll().then((histories)=>{
        res.json({message: "Fetch all success",data: histories})
    })
})

app.post("/histories",(req,res)=>{
    const {result,score,user_id} = req.body;
        User_History.create({
            result,
            score,
            user_id
        }).then((history)=>{
            res.json({message: "create succcess",data:history})
        })
    }
)

app.put("/histories/:id",(req,res)=>{
    const {id} = req.params;
    const {result,score,user_id} = req.body;
    User_History.update({
        result,
        score,
        user_id
        },{where: {user_id:id}})
    .then(()=>{
        res.json({message: "update success"})
    })
    .catch(()=>{
        res.json({message:"gagal mengupdate artikel"})
    })
})

app.delete("/histories/:id",(req,res)=>{
    const {id} = req.params;
    User_History.destroy({where: {user_id: id}})
    .then(()=>{     
        res.json({message:"delete success"})
    })
    .catch(()=>{
        res.json({message:"gagal menghapus data"})
    })
})

app.listen(8001,() => console.log(`listening at http://localhost:${8001}`))